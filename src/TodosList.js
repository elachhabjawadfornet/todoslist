import { useState } from 'react';
import Form from './components/Form';
import Todo from './components/Todo';
import Button from './components/Button';


function TodosList() {

  let [todos, setTodos] = useState([
    {
      id: 1,
      text: 'Learn Js',
      complete: true
    },
    {
      id: 2,
      text: 'NodeJs',
      complete: false
    },
    {
      id: 3,
      text: 'MongoDB',
      complete: false
    },
    {
      id: 4,
      text: 'NestJs',
      complete: false
    },
    {
      id: 5,
      text: 'Learn Reactjs',
      complete: true
    },
    {
      id: 6,
      text: 'Learn VueJs',
      complete: false
    },


  ]);

  const [choise, setChoise] = useState('all');
  const [toggleAllcompleted, setToggleAllcompleted] = useState(true);

  const saveTodo = todo => setTodos(todos => [todo, ...todos]);
  const removeTodo = id => {

   
    if (window.confirm('Delete the todos ?')) {
      setTodos(todos => todos.filter(todo => todo.id !== id))
    }

  };

  if (choise === 'active') {
    todos = todos.filter(todo => !todo.complete)
  } else if (choise === 'complete') {
    todos = todos.filter(todo => todo.complete)
  }


  const toggelCompleteTodo = (id) => {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            complete: !todo.complete
          }
        } else
          return todo
      })
    )
  }



  return (
    <div className="container">
      <div className="card">
        <div className="card-header">
          <h1>to-do list </h1>
          <span className="state">{choise}</span>
        </div>

        <div className="card-body">
          <Form handelStoreTodo={saveTodo} />
          <ul className="todos">
            {todos.map(todo =>

              <Todo key={todo.id} {...todo} onRemoveTodo={removeTodo} onToggleCompleteTodo={toggelCompleteTodo} />

            )}


          </ul>
        </div>

        <div className="card-footer">

          <Button
            classes={['btn-primary']}
            label="All"
            handleFn={() => setChoise('all')}
          />

          <Button
            classes={['btn-success']}
            label="Active"
            handleFn={() => setChoise('active')}
          />

          <Button
            classes={['btn-warning']}
            label="Complete"
            handleFn={() => setChoise('complete')}
          />



          <Button
            classes={['btn-info']}
            label="Remove all completed todos"
            handleFn={() => setTodos(todos.filter(todo => !todo.complete))}
          />



          <Button
            classes={['btn-dark']}
            label={`Toggel all completed todos : ${toggleAllcompleted}`}
            handleFn={() => {

              setTodos(todos.map(todo => ({
                ...todo,
                complete: toggleAllcompleted
              })

              ))

              setToggleAllcompleted(!toggleAllcompleted);
            }} />




        </div>

      </div>
    </div>






  );
}

export default TodosList;
