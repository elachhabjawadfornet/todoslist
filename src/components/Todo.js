import React from 'react'


function Todo({ id, text, complete, onRemoveTodo, onToggleCompleteTodo }) {


    return (
        <li className={`todo ${complete ? "underline" : null}`}>
            <span className={complete ? "text-primary" : null} htmlFor={text} onClick={() => onToggleCompleteTodo(id)} >{text}</span>
            <span onClick={() => onRemoveTodo(id)} className="remove">X</span>
        </li>
    )
}

export default Todo