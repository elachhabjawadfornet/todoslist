

function Button({ classes, label,handleFn,}) {
  return (
      <button className={`btn ${classes.join(' ')}`} onClick={handleFn}>{label}</button>
  )
}

export default Button