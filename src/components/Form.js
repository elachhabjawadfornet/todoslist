import React, { useState } from 'react';




function Form({ handelStoreTodo }) {

    const [text, setText] = useState('');
    const [error,setError] = useState({});
    const handelSubmit = (e) => {
        e.preventDefault();

        if(text.trim() === ""){
            setError({ text:"le champ est requis"});
            return;
        }

        handelStoreTodo({
            id: Math.random(),
            text: text,
            complete: false
        });

        setText('');
        setError({});
    }
  
    return (
       
        <form onSubmit={handelSubmit}>
            <div className="input-form">
                <input placeholder="Add Your Todo" value={text} onChange={(e) => setText(e.target.value)} />
                <button className="btn btn-primary">Add</button>
            </div>
            {error.text && <span className="errors">{error.text}</span>}
            
        </form>

    )
}

export default Form